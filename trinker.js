const { forEach, indexOf } = require("./people");

module.exports = {
  title: function () {
    return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow;
  },

  line: function (title = "=") {
    return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black;
  },

  allMale: function (p) {
    let n = [];
    for (x of p) {
      if (x.gender === "Male") {
        n.push(x);
      }
    }
    return n;
  },

  allFemale: function (p) {
    let n = [];
    for (x of p) {
      if (x.gender === "Female") {
        n.push(x);
      }
    }
    return n;
  },

  nbOfMale: function (p) {
    let n = 0;
    for (let x of p) {
      if (x.gender == "Male") n++;
    }
    return n;
  },

  nbOfFemale: function (p) {
    let n = 0;
    for (let x of p) {
      if (x.gender == "Female") n++;
    }
    return n;
  },

  nbOfMaleInterest: function (p) {
    let n = 0;
    for (let x of p) {
      if (x.looking_for == "M") n++;
    }
    return n;
  },

  nbOfFemaleInterest: function (p) {
    let n = 0;
    for (let x of p) {
      if (x.looking_for == "F") n++;
    }
    return n;
  },

  nbOfpeople2000$: function (p) {
    let n = 0;
    for (let x of p) {
      result = x.income;
      value = parseFloat(result.slice(1));
      if (value > 2000) {
        n++;
      }
    }
    return n;
  },

  nbOfpeopleDrama: function (p) {
    let n = 0;
    for (let x of p) {
      if (x.pref_movie.includes("Drama")) n++;
    }
    return n;
  },

  nbOfFemaleSF: function (p) {
    let n = 0;
    for (let x of this.allFemale(p)) {
      if (x.pref_movie.includes("Sci-Fi")) n++;
    }
    return n;
  },

  nbOfpeopleDocumentary1482$: function (p) {
    let n = 0;
    for (let x of p) {
      if (
        parseFloat(x.income.substring(1)) > 1482 &&
        x.pref_movie.includes("Documentary")
      ) {
        n++;
      }
    }
    return n;
  },

  listOfPeople4000$: function (p) {
    let n = [];
    for (let x of p) {
      result = x.income;
      value = parseFloat(result.slice(1));
      if (value > 4000) {
        n.push({
          prenom: x.first_name,
          nom: x.last_name,
          id: x.id,
          rev: x.income,
        });
      }
    }
    return n;
  },

  maleriches: function (p) {
    let hommeleplusriche = 0;
    for (let x of this.allMale(p)) {
      result = x.income;
      value = parseFloat(result.slice(1));
      if (value > hommeleplusriche) {
        hommeleplusriche = value;
        id_revenu = x.id;
        name_revenu = x.last_name;
      }
    }
    return (
      "ID : " +
      id_revenu +
      " Nom : " +
      name_revenu +
      " Revenu : " +
      hommeleplusriche +
      "$"
    );
  },

  salaireMoyen: function (p) {
    let total = 0;
    for (let x of p) {
      revenu = parseFloat(x.income.substring(1));
      total += revenu;
    }
    return Math.round(total / p.length);
  },

  salaireMedian: function (p) {
    let n = [];
    let tab = p.length;
    let milieu = parseInt(tab / 2);
    for (let x of p) {
      let salaire = parseFloat(x.income.substring(1));
      n.push(salaire);
      n.sort((a, b) => a - b);
    }
    return Math.round((n[milieu] + n[milieu + 1]) / 2);
  },

  nbHemNord: function (p) {
    let n = 0;
    for (let x of p) {
      if (x.latitude > 0) {
        n++;
      }
    }
    return n;
  },

  allOf_HemSud: function (p) {
    let perss = [];
    for (let x of p) {
      if (x.latitude < 0) {
        perss.push(x);
      }
    }
    return perss;
  },

  salaireMoyenHemSud: function (p) {
    let sud = this.allOf_HemSud(p);
    return this.salaireMoyen(sud);
  },

  pythagore: function (a, b) {
    let x = Math.sqrt((b.long - a.long) ** 2 + (b.lat - a.lat) ** 2);
    return x;
  },

  closeToCawt: function (p) {
    let a = { long: 0, lat: 0 };
    let grand = 999999999;
    let leplusproche = [];
    for (let i of p) {
      let b = { long: i.longitude, lat: i.latitude };

      if (i.last_name == "Cawt") {
        a = { long: i.longitude, lat: i.latitude };
      }
      if (this.pythagore(a, b) < grand && this.pythagore(a, b) !== 0) {
        grand = this.pythagore(a, b);
        leplusproche = [i.last_name, i.id];
      }
    }
    return leplusproche;
  },

  closeToBrach: function (p) {
    let a = { long: 0, lat: 0 };
    let grand = 999999999;
    let leplusproche = [];
    for (let i of p) {
      let b = { long: i.longitude, lat: i.latitude };

      if (i.last_name == "Brach") {
        a = { long: i.longitude, lat: i.latitude };
      }
      if (this.pythagore(a, b) < grand && this.pythagore(a, b) !== 0) {
        grand = this.pythagore(a, b);
        leplusproche = [i.last_name, i.id];
      }
    }
    return leplusproche;
  },

  dixPersCloseToBoshard: function (p) {
    let dist = [];
    let latBoshard = 57.6938555;
    let longBoshard = 11.9704401;
    for (let x of p) {
      let a = latBoshard - x.latitude;
      let b = longBoshard - x.longitude;
      let distance_entre_eux = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
      dist.push({ distance: distance_entre_eux, nom: x.last_name, id: x.id });
      dist.sort((a, b) => a.distance - b.distance);
    }

    let lesdix = [];
    for (i = 1; i < 11; i++) {
      lesdix.push(dist[i]);
    }
    return lesdix;
  },

  nameIdGoogle: function (p) {
    let list = [];
    const google = p.filter((p) => p.email.includes("google"));
    for (let x of google) {
      list.push({ name: x.last_name, id: x.id });
    }
    return list;
  },

  older: function (p) {
    let people = [];
    for (let x of p) {
      people.push({ name: x.last_name, age: this.calcAge(x) });
      people.sort(function (a, b) {
        return b.age - a.age;
      });
    }
    return people[0];
  },

  younger: function (p) {
    let people = [];
    for (let x of p) {
      people.push({ name: x.last_name, age: this.calcAge(x) });
      people.sort(function (a, b) {
        return a.age - b.age;
      });
    }
    return people[0];
  },

  moyenneDifferenceAge: function (p) {
    let tableau = [];
    let compare = [];
    const reduire = (accumulator, curr) => accumulator + curr;
    for (pers of p) {
      age = this.calcAge(pers);
      tableau.push(pers.age);
      for (pers_deux of p) {
        age2 = this.calcAge(pers_deux);
        tableau.push(pers_deux.age2);
        if (age > age2) {
          compare.push(age - age2);
        } else {
          compare.push(age2 - age);
        }
      }
    }
    return compare.reduce(reduire) / compare.length;
  },

  famousPrefMovie: function (p) {
//    tm = a.m.split("|");
//    list = {};
//  for(m of tm){
//    if(list[m]){
//      list[m]+=1
//    } else {
//      list[m]=1
//    }
    

//  }
  },

  orderPopularityFilm: function (p) {},

  ListFilmAndNbPref: function (p) {},

  calcAge: function (p) {
    let dob = p.date_of_birth;
    let birthday = new Date(dob);
    return ~~((Date.now() - birthday) / 31557600000);
  },

  ageMoyenHomFilmNoir: function (p) {
    total = [];
    const reducer = (previousValue, currentValue) =>
      previousValue + currentValue;
    for (let i of this.allMale(p)) {
      if (i.pref_movie.includes("Film-Noir")) {
        total.push(this.calcAge(i));
      }
    }

    return total.reduce(reducer) / total.length;
  },

  ageMoyenFemFilmNoirParisRevMoinsHom: function (p) {},

  homForHomCloseToPrefFilm: function (p) {},

  listFemHomSamePrefFilm: function (p) {},

  match: function (p) {
    return "not implemented".red;
  },
};
